# elk

Elasticsearch, Logstash, Kibana
#
https://www.elastic.co/what-is/elk-stack
#
#!/bin/bash -x
dir_yaml=/home/sergio/work/git
minikube delete -p devops
rm -rf /home/sergio/.kube
rm -rf /home/sergio/.minikube

minikube start --memory 11264 --cpus 4 --disk-size='150000mb' -p devops
sleep 180

kubectl get pods -n kube-system

cd /home/sergio/work/git/kubernetes-prometheus-kafka-adapter/
eval $(minikube docker-env -p devops)
docker build -t prometheus-kafka-adapter:v1 .

kubectl create -f ${dir_yaml}/kubernetes-kafka/01-namespace/ && \
kubectl create -f ${dir_yaml}/kubernetes-kafka/02-rbac-namespace-default && \
kubectl create -f ${dir_yaml}/kubernetes-kafka/03-zookeeper ; sleep 120 && \
kubectl create -f ${dir_yaml}/kubernetes-kafka/05-kafka ; sleep 120 && \
kubectl create -f ${dir_yaml}/kubernetes-kafka/06-kasfka-manager ; sleep 60 && \
kubectl create -f ${dir_yaml}/kubernetes-kafka/kafka-cli.yml ; sleep 120 && \ 
kubectl get pods -n inmetrics && \ 
/home/sergio/work/scripts/cria-topics.sh cria && \
kubectl create -f ${dir_yaml}/kubernetes-elasticsearch/ ; sleep 120 && \
kubectl create -f ${dir_yaml}/kubernetes-logstash/ ; sleep 120 && \
kubectl create -f ${dir_yaml}/kubernetes-prometheus-kafka-adapter/ ; sleep 60 && \
kubectl get pods -n inmetrics && \
kubectl create -f ${dir_yaml}/kubernetes-prometheus/ ; sleep 60 && \
kubectl create -f ${dir_yaml}/kubernetes-filebeat/  ; sleep 60 && \
kubectl get pods -n monitoring
